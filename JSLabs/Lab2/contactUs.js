function validateContactUsForm() {
	var nameField = document.forms.contactUsForm.name.value;
	if (nameField === null || nameField === "") {
		alert("Please provide your name.");
		return false;
	}

	var emailField = document.forms.contactUsForm.email.value;
	var phoneField = document.forms.contactUsForm.phone.value;
	if (emailField === "" && phoneField === "") {
		alert("Please provide either a phone number or email address for us to contact you at.");
		return false;
	}

	var additionalInfo = document.forms.contactUsForm.addInfo.value;
	if (document.getElementById("other").selected === true) {
		if (additionalInfo === "") {
			alert("Please provide additional information when selecting the 'Other' option.");
			return false;
		}
	}

	var monContact = document.forms.contactUsForm.mon.checked;
	var tueContact = document.forms.contactUsForm.tue.checked;
	var wedContact = document.forms.contactUsForm.wed.checked;
	var thuContact = document.forms.contactUsForm.thu.checked;
	var friContact = document.forms.contactUsForm.fri.checked;
	if (monContact === true || tueContact === true || wedContact === true || thuContact === true || friContact === true) {}
	else {
		alert("Please select at least one day on which you can best be contacted.");
		return false;
	}
}